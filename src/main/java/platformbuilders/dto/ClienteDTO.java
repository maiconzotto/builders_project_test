package platformbuilders.dto;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import platformbuilders.model.Cliente;
import platformbuilders.util.Util;

@Data
public class ClienteDTO {

	private Long id;

	private String nome;

	@JsonFormat(pattern = "dd/MM/yyyy")
	private String cpf;

	@DateTimeFormat(iso = ISO.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataNascimento;

	public Integer getIdade() {
		if(dataNascimento != null)
			return Period.between(dataNascimento, Util.convertToLocalDateViaInstant(new Date())).getYears();
		else
			return null;
	}

	public static ClienteDTO convertToDto(Cliente t) {

		ModelMapper modelMapper = new ModelMapper();
		ClienteDTO dto = modelMapper.map(t, ClienteDTO.class);

		return dto;
	}

	public static Cliente convertToEntity(ClienteDTO dto) {
		ModelMapper modelMapper = new ModelMapper();
		Cliente t = modelMapper.map(dto, Cliente.class);

		return t;
	}
}
