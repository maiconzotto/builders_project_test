package platformbuilders.repository;

import org.springframework.data.domain.Pageable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import platformbuilders.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
    List<Cliente> findByNomeAndCpf(String nome, String cpf, Pageable pageable);
    
    List<Cliente> findByNomeContains(String nome, Pageable pageable);
    
    List<Cliente> findByCpf(String cpf, Pageable pageable);
}
