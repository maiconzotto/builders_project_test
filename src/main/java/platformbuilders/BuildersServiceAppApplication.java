package platformbuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildersServiceAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildersServiceAppApplication.class, args);
	}

}
