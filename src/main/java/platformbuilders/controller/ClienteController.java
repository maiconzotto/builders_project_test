package platformbuilders.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import platformbuilders.dto.ClienteDTO;
import platformbuilders.model.Cliente;
import platformbuilders.service.ClienteService;

@RestController
@RequestMapping(value = "/api", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping(value = "/cliente/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ClienteDTO> getClienteById(@PathVariable(value = "id") Long ClienteId) throws Exception {
		return ResponseEntity.ok().body(ClienteDTO.convertToDto(clienteService.findById(ClienteId)));
	}

	@GetMapping(value = "/cliente", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<ClienteDTO> getAllClientes(@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "cpf", required = false) String cpf, @RequestParam(defaultValue = "0") Integer pagina, @RequestParam(defaultValue = "2") Integer tamanho) {

		if (nome != null && cpf != null)
			return clienteService.findByNomeAndCpf(nome, cpf, pagina, tamanho).stream().map(f -> ClienteDTO.convertToDto(f))
					.collect(Collectors.toList());
		else if (nome != null)
			return clienteService.findByNome(nome, pagina, tamanho).stream().map(f -> ClienteDTO.convertToDto(f))
					.collect(Collectors.toList());
		else if (cpf != null)
			return clienteService.findByCpf(cpf, pagina, tamanho).stream().map(f -> ClienteDTO.convertToDto(f))
					.collect(Collectors.toList());
		else
			return clienteService.findAll(pagina, tamanho).stream().map(f -> ClienteDTO.convertToDto(f)).collect(Collectors.toList());
	}

	@PostMapping(value = "/cliente", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ClienteDTO> createCliente(@Valid @RequestBody ClienteDTO clienteDTO) {

		Cliente cliente = ClienteDTO.convertToEntity(clienteDTO);
		try {
			cliente = clienteService.save(cliente);
			return new ResponseEntity<ClienteDTO>(ClienteDTO.convertToDto(cliente), HttpStatus.CREATED);
		} catch (DataIntegrityViolationException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@PutMapping(value = "/cliente/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ClienteDTO> putCliente(@PathVariable(value = "id") Long ClienteId,
			@Valid @RequestBody ClienteDTO clienteDTO) throws Exception {
		
		Cliente cliente = clienteService.findById(ClienteId);
		
	    if( cliente == null){
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
	    }

		Cliente novoCliente = ClienteDTO.convertToEntity(clienteDTO);

		try {
			novoCliente = clienteService.put(ClienteId, novoCliente);
			return new ResponseEntity<ClienteDTO>(ClienteDTO.convertToDto(novoCliente), HttpStatus.CREATED);
		} catch (DataIntegrityViolationException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	@PatchMapping(value = "/cliente/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ClienteDTO> patchCliente(@PathVariable(value = "id") Long clienteId,
			@Valid @RequestBody ClienteDTO clienteDTO) throws Exception {
		
		Cliente oldCliente = clienteService.findById(clienteId);
		
	    if( oldCliente == null){
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
	    }
	    
	    Cliente newCliente = ClienteDTO.convertToEntity(clienteDTO);
	    
	    try {
	    	oldCliente = clienteService.patchCliente(clienteId, newCliente, oldCliente);
			return new ResponseEntity<ClienteDTO>(ClienteDTO.convertToDto(oldCliente), HttpStatus.CREATED);
		} catch (DataIntegrityViolationException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}

	@DeleteMapping(value = "/cliente/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public Map<String, Boolean> deleteCliente(@PathVariable(value = "id") Long ClienteId) throws Exception {
		clienteService.delete(ClienteId);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
