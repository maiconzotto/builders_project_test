package platformbuilders.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import platformbuilders.model.Cliente;
import platformbuilders.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
    private ClienteRepository clienteRepository;
	
	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	} 
	
	public Cliente put(long clienteId, Cliente newCliente) throws Exception {
		newCliente.setId(clienteId);	
		return clienteRepository.save(newCliente);
	}
	
	public void delete(long clienteId) throws Exception {
		
		Cliente cliente = clienteRepository.findById(clienteId).orElseThrow(() -> new Exception("Cliente not found for this id :: " + clienteId));
		
		clienteRepository.delete(cliente);
	}
	
	public Cliente findById(long clienteId) throws Exception {
		return clienteRepository.findById(clienteId).orElseThrow(() -> new Exception("Cliente not found for this id :: " + clienteId));
	}
	
	public List<Cliente> findAll(Integer pagina, Integer tamanho) {
		
		Pageable paging = PageRequest.of(pagina, tamanho);
		
		Page<Cliente> resultado = clienteRepository.findAll(paging);
		
		if(resultado.hasContent()) {
            return resultado.getContent();
        } else {
            return new ArrayList<Cliente>();
        }
	}
	
	public List<Cliente> findByNomeAndCpf(String nome, String cpf, Integer pagina, Integer tamanho){
		
		Pageable paging = PageRequest.of(pagina, tamanho);
		
		return clienteRepository.findByNomeAndCpf(nome, cpf, paging);
	}
	
	public List<Cliente> findByNome(String nome, Integer pagina, Integer tamanho){
		
		Pageable paging = PageRequest.of(pagina, tamanho);
		
		return clienteRepository.findByNomeContains(nome, paging);
	}
	
	public List<Cliente> findByCpf(String cpf, Integer pagina, Integer tamanho){
		
		Pageable paging = PageRequest.of(pagina, tamanho);
		
		return clienteRepository.findByCpf(cpf, paging);
	}
	
	public Cliente patchCliente(long clienteId, Cliente newCliente, Cliente oldCliente) throws Exception {
		
		if(newCliente.getNome() != null)
			oldCliente.setNome(newCliente.getNome());
		if(newCliente.getCpf() != null)
			oldCliente.setCpf(newCliente.getCpf());
		if(newCliente.getDataNascimento() != null) {
			oldCliente.setDataNascimento(newCliente.getDataNascimento());			
		}
		
		return clienteRepository.save(oldCliente);
	}
}
